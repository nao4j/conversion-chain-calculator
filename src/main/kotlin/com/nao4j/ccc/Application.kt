package com.nao4j.ccc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories

@SpringBootApplication
@EnableJdbcRepositories
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
