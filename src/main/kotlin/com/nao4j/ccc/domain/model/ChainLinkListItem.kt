package com.nao4j.ccc.domain.model

import com.nao4j.ccc.dal.row.ChainLinkRow

data class ChainLinkListItem(
    val id: Long,
    val parentId: Long? = null,
    val currency: String
)

fun chainLinkListItemFrom(chainLinkRow: ChainLinkRow) = ChainLinkListItem(
    id = chainLinkRow.id!!,
    parentId = chainLinkRow.parentId,
    currency = chainLinkRow.currency
)
