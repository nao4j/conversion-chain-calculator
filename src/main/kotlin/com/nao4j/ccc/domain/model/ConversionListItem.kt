package com.nao4j.ccc.domain.model

import com.nao4j.ccc.dal.row.ConversionRow
import java.math.BigDecimal
import java.time.LocalDateTime

data class ConversionListItem(
    val id: Long,
    val createdAt: LocalDateTime,
    val amount: BigDecimal,
    val currencyFrom: String,
    val rate: BigDecimal,
    val currencyTo: String,
)

fun conversionListItemFrom(conversionRow: ConversionRow) = ConversionListItem(
    id = conversionRow.id!!,
    createdAt = conversionRow.createdAt,
    amount = conversionRow.amount,
    currencyFrom = conversionRow.currencyFrom,
    currencyTo = conversionRow.currencyTo,
    rate = conversionRow.rate
)
