package com.nao4j.ccc.domain.model

import com.nao4j.ccc.dal.row.ChainLinkRow
import java.math.BigDecimal

data class ChainLinkDetails(
    val id: Long,
    val parentId: Long? = null,
    val rate: BigDecimal,
    val percentageFee: BigDecimal? = null,
    val fixedFee: BigDecimal? = null,
    val currency: String
)

fun chainLinkDetailsFrom(linkRow: ChainLinkRow) = ChainLinkDetails(
    id = linkRow.id!!,
    parentId = linkRow.parentId,
    rate = linkRow.rate,
    percentageFee = linkRow.percentageFee,
    fixedFee = linkRow.fixedFee,
    currency = linkRow.currency
)
