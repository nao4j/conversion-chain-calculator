package com.nao4j.ccc.domain.model

import com.nao4j.ccc.dal.row.ChainRow
import com.nao4j.ccc.dal.row.ConversionRow
import java.math.BigDecimal
import java.time.LocalDateTime

data class ConversionDetails(
    val id: Long,
    val createdAt: LocalDateTime,
    val amount: BigDecimal,
    val currencyFrom: String,
    val rate: BigDecimal,
    val currencyTo: String,
    val chains: List<ChainListItem> = emptyList()
)

fun conversionDetailsFrom(conversionRow: ConversionRow, chainRows: List<ChainRow> = emptyList()) = ConversionDetails(
    id = conversionRow.id!!,
    createdAt = conversionRow.createdAt,
    amount = conversionRow.amount,
    currencyFrom = conversionRow.currencyFrom,
    rate = conversionRow.rate,
    currencyTo = conversionRow.currencyTo,
    chains = chainRows.map(::chainListItemFrom)
)
