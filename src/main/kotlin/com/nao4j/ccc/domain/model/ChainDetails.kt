package com.nao4j.ccc.domain.model

import com.nao4j.ccc.dal.row.ChainLinkRow
import com.nao4j.ccc.dal.row.ChainRow
import com.nao4j.ccc.domain.model.ChainDetails.ChainStatusResponse
import java.math.BigDecimal

data class ChainDetails(
    val id: Long,
    val conversionId: Long,
    val name: String,
    val status: ChainStatusResponse,
    val lossRate: BigDecimal? = null,
    val links: List<ChainLinkListItem>
) {
    enum class ChainStatusResponse {
        CREATED,
        IN_PROGRESS,
        COMPLETED
    }
}

fun chainDetailsFrom(chainRow: ChainRow, linkRows: List<ChainLinkRow> = emptyList()) = ChainDetails(
    id = chainRow.id!!,
    conversionId = chainRow.conversionId,
    name = chainRow.name,
    status = ChainStatusResponse.valueOf(chainRow.status.name),
    lossRate = chainRow.lossRate,
    links = linkRows.map(::chainLinkListItemFrom)
)
