package com.nao4j.ccc.domain.model

import com.nao4j.ccc.dal.row.ChainRow
import com.nao4j.ccc.domain.model.ChainListItem.ChainStatus
import java.math.BigDecimal

data class ChainListItem(
    val id: Long,
    val name: String,
    val status: ChainStatus,
    val lossRate: BigDecimal? = null
) {
    enum class ChainStatus {
        CREATED,
        IN_PROGRESS,
        COMPLETED
    }
}

fun chainListItemFrom(chainRow: ChainRow) = ChainListItem(
    id = chainRow.id!!,
    name = chainRow.name,
    status = ChainStatus.valueOf(chainRow.status.name),
    lossRate = chainRow.lossRate
)
