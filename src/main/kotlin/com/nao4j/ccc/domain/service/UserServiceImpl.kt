package com.nao4j.ccc.domain.service

import com.nao4j.ccc.dal.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(private val userRepository: UserRepository) : UserService {
    override fun getIdByUsername(username: String): Long? = userRepository.findByUsername(username)?.id
}
