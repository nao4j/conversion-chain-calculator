package com.nao4j.ccc.domain.service

interface UserService {

    fun getIdByUsername(username: String): Long?
}
