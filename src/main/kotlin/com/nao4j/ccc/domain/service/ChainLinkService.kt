package com.nao4j.ccc.domain.service

import com.nao4j.ccc.domain.model.ChainLinkDetails
import java.math.BigDecimal

interface ChainLinkService {
    fun getDetails(userId: Long, conversionId: Long, chainId: Long, linkId: Long): ChainLinkDetails?

    fun create(
        userId: Long,
        conversionId: Long,
        chainId: Long,
        parentId: Long?,
        rate: BigDecimal,
        percentageFee: BigDecimal?,
        fixedFee: BigDecimal?,
        currency: String
    ): ChainLinkDetails

    fun delete(userId: Long, conversionId: Long, chainId: Long, linkId: Long): Boolean
}
