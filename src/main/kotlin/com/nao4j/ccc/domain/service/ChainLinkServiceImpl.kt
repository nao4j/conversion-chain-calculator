package com.nao4j.ccc.domain.service

import com.nao4j.ccc.dal.repository.ChainLinkRepository
import com.nao4j.ccc.dal.repository.ChainRepository
import com.nao4j.ccc.dal.row.ChainLinkRow
import com.nao4j.ccc.dal.row.ChainRow.ChainStatusEnum.COMPLETED
import com.nao4j.ccc.dal.row.ChainRow.ChainStatusEnum.IN_PROGRESS
import com.nao4j.ccc.domain.model.ChainLinkDetails
import com.nao4j.ccc.domain.model.chainLinkDetailsFrom
import com.nao4j.ccc.exception.CreationChainLinkException
import com.nao4j.ccc.exception.RemovalChainLinkException
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class ChainLinkServiceImpl(
    private val chainRepository: ChainRepository,
    private val chainLinkRepository: ChainLinkRepository
) : ChainLinkService {
    override fun getDetails(userId: Long, conversionId: Long, chainId: Long, linkId: Long): ChainLinkDetails? =
        chainLinkRepository.findByUserIdAndConversionIdAndChainIdAndId(userId, conversionId, chainId, linkId)
            ?.let(::chainLinkDetailsFrom)

    override fun create(
        userId: Long,
        conversionId: Long,
        chainId: Long,
        parentId: Long?,
        rate: BigDecimal,
        percentageFee: BigDecimal?,
        fixedFee: BigDecimal?,
        currency: String
    ): ChainLinkDetails {
        val chain = chainRepository.findByUserIdAndConversionIdAndId(userId, conversionId, chainId)
        if (chain?.status == COMPLETED) {
            throw CreationChainLinkException() // TODO: chain completed
        }
        if (!chainLinkRepository.existsByUserIdAndConversionIdAndChainId(userId, conversionId, chainId)) {
            chain?.copy(status = IN_PROGRESS)?.let(chainRepository::save)
        }
        return ChainLinkRow(
            userId = userId,
            conversionId = conversionId,
            chainId = chainId,
            parentId = parentId,
            rate = rate,
            percentageFee = percentageFee,
            fixedFee = fixedFee,
            currency = currency
        ).let(chainLinkRepository::save)
            .let(::chainLinkDetailsFrom)
    }

    override fun delete(userId: Long, conversionId: Long, chainId: Long, linkId: Long): Boolean {
        if (chainRepository.findByUserIdAndConversionIdAndId(userId, conversionId, chainId)?.status == COMPLETED) {
            throw RemovalChainLinkException() // TODO: chain is completed
        }
        return chainLinkRepository.findByUserIdAndConversionIdAndChainIdAndId(userId, conversionId, chainId, linkId)
            ?.let(chainLinkRepository::delete)
            ?.let { true } ?: false
    }
}
