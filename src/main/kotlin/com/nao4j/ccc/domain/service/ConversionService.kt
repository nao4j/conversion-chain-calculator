package com.nao4j.ccc.domain.service

import com.nao4j.ccc.domain.model.ConversionDetails
import com.nao4j.ccc.domain.model.ConversionListItem
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.math.BigDecimal

interface ConversionService {
    fun getPage(userId: Long, pageable: Pageable): Page<ConversionListItem>

    fun getDetails(userId: Long, conversionId: Long): ConversionDetails?

    fun create(
        userId: Long,
        amount: BigDecimal,
        currencyFrom: String,
        rate: BigDecimal,
        currencyTo: String
    ): ConversionDetails

    fun delete(userId: Long, conversionId: Long): Boolean
}
