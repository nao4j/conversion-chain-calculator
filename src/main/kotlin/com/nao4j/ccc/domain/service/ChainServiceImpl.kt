package com.nao4j.ccc.domain.service

import com.nao4j.ccc.dal.repository.ChainLinkRepository
import com.nao4j.ccc.dal.repository.ChainRepository
import com.nao4j.ccc.dal.repository.ConversionRepository
import com.nao4j.ccc.dal.row.ChainLinkRow
import com.nao4j.ccc.dal.row.ChainRow
import com.nao4j.ccc.dal.row.ChainRow.ChainStatusEnum.COMPLETED
import com.nao4j.ccc.domain.model.ChainDetails
import com.nao4j.ccc.domain.model.chainDetailsFrom
import com.nao4j.ccc.exception.ChainCompletionException
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.math.RoundingMode.HALF_DOWN
import java.math.RoundingMode.HALF_UP
import kotlin.Int.Companion.MAX_VALUE

@Service
class ChainServiceImpl(
    private val conversionRepository: ConversionRepository,
    private val chainRepository: ChainRepository,
    private val chainLinkRepository: ChainLinkRepository
) : ChainService {
    @Transactional
    override fun getDetails(userId: Long, conversionId: Long, chainId: Long): ChainDetails? =
        chainRepository.findByUserIdAndConversionIdAndId(userId, conversionId, chainId)
            ?.let { row ->
                chainDetailsFrom(
                    chainRow = row,
                    linkRows = getConsistentChain(userId = userId, conversionId = conversionId, chainId = chainId)
                )
            }

    @Transactional
    override fun create(userId: Long, conversionId: Long, name: String): ChainDetails =
        ChainRow(userId = userId, conversionId = conversionId, name = name)
            .let(chainRepository::save)
            .let(::chainDetailsFrom)

    @Transactional
    override fun complete(userId: Long, conversionId: Long, chainId: Long): ChainDetails? {
        val conversion = conversionRepository.findByUserIdAndId(userId, conversionId)
        val chain = chainRepository.findByUserIdAndConversionIdAndId(userId, conversionId, chainId)
        val links = getConsistentChain(userId = userId, conversionId = conversionId, chainId = chainId)
        validateCompletion(
            conversionCurrency = conversion?.currencyTo,
            chainFound = chain != null,
            latestLinkCurrency = links.lastOrNull()?.currency
        )

        return chainRepository.findByUserIdAndConversionIdAndId(userId, conversionId, chainId)
            ?.copy(
                lossRate = calculateLossRate(conversion!!.amount, conversion.rate, links),
                status = COMPLETED
            )?.let(chainRepository::save)
            ?.let { row ->
                chainDetailsFrom(
                    chainRow = row,
                    linkRows = getConsistentChain(userId = userId, conversionId = conversionId, chainId = chainId)
                )
            }
    }

    @Transactional
    override fun delete(userId: Long, conversionId: Long, chainId: Long): Boolean =
        chainRepository.findByUserIdAndConversionIdAndId(userId, conversionId, chainId)
            ?.let(chainRepository::delete)
            ?.let { true } ?: false

    fun validateCompletion(conversionCurrency: String?, chainFound: Boolean, latestLinkCurrency: String?) {
        if (conversionCurrency == null) {
            throw ChainCompletionException() // TODO: conversion not found
        }
        if (!chainFound) {
            throw ChainCompletionException() // TODO: chain not found
        }
        if (latestLinkCurrency == null) {
            throw ChainCompletionException() // TODO: chain empty
        }
        if (conversionCurrency != latestLinkCurrency) {
            throw ChainCompletionException() // TODO: chain not complete
        }
    }

    private fun getConsistentChain(userId: Long, conversionId: Long, chainId: Long): List<ChainLinkRow> =
        chainLinkRepository.findAllByUserIdAndConversionIdAndChainId(
            userId = userId,
            conversionId = conversionId,
            chainId = chainId,
            pageable = Pageable.ofSize(MAX_VALUE)
        ).content.sortedWith { link1, link2 ->
            if (link1.parentId == link2.id) {
                1
            } else if (link1.parentId == null) {
                -1
            } else if (link2.parentId == null) {
                1
            } else {
                link1.parentId.compareTo(link2.parentId)
            }
        }

    private fun calculateLossRate(amount: BigDecimal, currentRate: BigDecimal, links: List<ChainLinkRow>): BigDecimal {
        val expectedAmount = amount.divide(currentRate, 8, HALF_DOWN)
        var actualAmount = amount
        for (link in links) {
            if (link.percentageFee != null) {
                actualAmount = actualAmount.minus(
                    actualAmount.multiply(
                        link.percentageFee.divide(BigDecimal.valueOf(100), 8, HALF_UP)
                    )
                )
            }
            if (link.fixedFee != null) {
                actualAmount = actualAmount.minus(link.fixedFee)
            }
            actualAmount = actualAmount.divide(link.rate, 8, HALF_DOWN)
        }
        return BigDecimal.valueOf(100)
            .minus(
                actualAmount.divide(
                    expectedAmount.multiply(BigDecimal.valueOf(0.01)), 8, HALF_UP
                )
            ).setScale(2, HALF_UP)
    }
}
