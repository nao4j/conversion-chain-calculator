package com.nao4j.ccc.domain.service

import com.nao4j.ccc.dal.repository.ChainRepository
import com.nao4j.ccc.dal.repository.ConversionRepository
import com.nao4j.ccc.dal.row.ConversionRow
import com.nao4j.ccc.domain.model.ConversionDetails
import com.nao4j.ccc.domain.model.ConversionListItem
import com.nao4j.ccc.domain.model.conversionDetailsFrom
import com.nao4j.ccc.domain.model.conversionListItemFrom
import com.nao4j.ccc.generator.DateTimeGenerator
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal

@Service
class ConversionServiceImpl(
    private val conversionRepository: ConversionRepository,
    private val chainRepository: ChainRepository,
    private val dateTimeGenerator: DateTimeGenerator
) : ConversionService {
    @Transactional
    override fun getPage(userId: Long, pageable: Pageable): Page<ConversionListItem> =
        conversionRepository.findAllByUserId(
            userId = userId,
            pageable = pageable
        ).map(::conversionListItemFrom)

    @Transactional
    override fun getDetails(userId: Long, conversionId: Long): ConversionDetails? =
        conversionRepository.findByUserIdAndId(userId, conversionId)
            ?.let { row ->
                conversionDetailsFrom(
                    conversionRow = row,
                    chainRows = chainRepository.findAllByUserIdAndConversionIdOrderByLossRateAscIdDesc(
                        userId = userId,
                        conversionId = row.id!!,
                        pageable = Pageable.ofSize(Integer.MAX_VALUE)
                    ).content
                )
            }

    @Transactional
    override fun create(
        userId: Long,
        amount: BigDecimal,
        currencyFrom: String,
        rate: BigDecimal,
        currencyTo: String
    ): ConversionDetails =
        ConversionRow(
            userId = userId,
            createdAt = dateTimeGenerator.generateLocalDateTimeNow(),
            amount = amount,
            currencyFrom = currencyFrom,
            rate = rate,
            currencyTo = currencyTo
        ).let(conversionRepository::save)
        .let(::conversionDetailsFrom)

    @Transactional
    override fun delete(userId: Long, conversionId: Long): Boolean =
        conversionRepository.findByUserIdAndId(userId, conversionId)
            ?.let(conversionRepository::delete)
            ?.let { true } ?: false
}
