package com.nao4j.ccc.domain.service

import com.nao4j.ccc.domain.model.ChainDetails

interface ChainService {
    fun getDetails(userId: Long, conversionId: Long, chainId: Long): ChainDetails?

    fun create(userId: Long, conversionId: Long, name: String): ChainDetails

    fun complete(userId: Long, conversionId: Long, chainId: Long): ChainDetails?

    fun delete(userId: Long, conversionId: Long, chainId: Long): Boolean
}
