package com.nao4j.ccc.web.page

import com.nao4j.ccc.domain.model.ConversionDetails

data class ConversionDetailsPage(
    val details: ConversionDetails? = null,
    val name: String? = null,
    val selectedChains: List<Long> = emptyList()
)
