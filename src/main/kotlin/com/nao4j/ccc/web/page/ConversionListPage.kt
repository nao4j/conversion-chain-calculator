package com.nao4j.ccc.web.page

import com.nao4j.ccc.domain.model.ConversionListItem
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import java.math.BigDecimal

data class ConversionListPage(
    val conversions: Page<ConversionListItem> = PageImpl(emptyList()),
    val amount: BigDecimal? = null,
    val currencyFrom: String? = null,
    val rate: BigDecimal? = null,
    val currencyTo: String? = null,
    val selectedConversions: List<Long> = emptyList()
)
