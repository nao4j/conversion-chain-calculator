package com.nao4j.ccc.web.page

import com.nao4j.ccc.domain.model.ChainDetails
import com.nao4j.ccc.domain.model.ChainLinkDetails
import com.nao4j.ccc.domain.model.ConversionDetails
import java.math.BigDecimal

data class ChainDetailsPage(
    val conversion: ConversionDetails? = null,
    val chain: ChainDetails? = null,
    val links: List<ChainLinkDetails?>? = null,
    val rate: BigDecimal? = null,
    val percentageFee: BigDecimal? = null,
    val fixedFee: BigDecimal? = null,
    val currency: String? = null,
    val error: String? = null
)
