package com.nao4j.ccc.web.controller

import com.nao4j.ccc.domain.service.ChainService
import com.nao4j.ccc.domain.service.ConversionService
import com.nao4j.ccc.domain.service.UserService
import com.nao4j.ccc.web.page.ConversionDetailsPage
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/conversion-details/{conversionId}")
class ConversionDetailsController(
    private val conversionService: ConversionService,
    private val chainService: ChainService,
    private val userService: UserService
) {

    // TODO: handle not found
    @GetMapping
    fun getPage(@PathVariable conversionId: Long, model: Model, authentication: Authentication): String {
        val userId = userService.getIdByUsername(authentication.name)
        val page = ConversionDetailsPage(
            details = conversionService.getDetails(userId!!, conversionId)
        )
        model.addAttribute("data", page)
        return "conversion-details"
    }

    @PostMapping(params = ["action=create"])
    fun create(
        @PathVariable conversionId: Long,
        @ModelAttribute("data") data: ConversionDetailsPage,
        authentication: Authentication
    ): String {
        val userId = userService.getIdByUsername(authentication.name)
        chainService.create(userId!!, conversionId, data.name!!)
        return "redirect:/conversion-details/{conversionId}"
    }

    @PostMapping(params = ["action=delete"])
    fun delete(
        @PathVariable conversionId: Long,
        @ModelAttribute("data") data: ConversionDetailsPage,
        authentication: Authentication
    ): String {
        val userId = userService.getIdByUsername(authentication.name)
        for (chainId in data.selectedChains) {
            chainService.delete(userId!!, conversionId, chainId)
        }
        return "redirect:/conversion-details/{conversionId}"
    }
}
