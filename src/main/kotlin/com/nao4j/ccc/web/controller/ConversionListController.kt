package com.nao4j.ccc.web.controller

import com.nao4j.ccc.domain.service.ConversionService
import com.nao4j.ccc.domain.service.UserService
import com.nao4j.ccc.web.page.ConversionListPage
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort.Direction.DESC
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import kotlin.Int.Companion.MAX_VALUE

@Controller
@RequestMapping("/", "/index", "/conversion-list")
class ConversionListController(
    private val conversionService: ConversionService,
    private val userService: UserService
) {

    @GetMapping
    fun getPage(model: Model, authentication: Authentication): String {
        val userId = userService.getIdByUsername(authentication.name)
        val page = ConversionListPage(
            conversions = conversionService.getPage(userId!!, PageRequest.of(0, MAX_VALUE, DESC, "createdAt"))
        )
        model.addAttribute("data", page)
        return "conversion-list"
    }

    @PostMapping(params = ["action=create"])
    fun create(@ModelAttribute("data") data: ConversionListPage, authentication: Authentication): String {
        val userId = userService.getIdByUsername(authentication.name)
        conversionService.create(
            userId = userId!!,
            amount = data.amount!!,
            currencyFrom = data.currencyFrom!!,
            rate = data.rate!!,
            currencyTo = data.currencyTo!!
        )
        return "redirect:/"
    }

    @PostMapping(params = ["action=delete"])
    fun delete(@ModelAttribute("data") data: ConversionListPage, authentication: Authentication): String {
        val userId = userService.getIdByUsername(authentication.name)
        for (conversionId in data.selectedConversions) {
            conversionService.delete(userId!!, conversionId)
        }
        return "redirect:/"
    }
}
