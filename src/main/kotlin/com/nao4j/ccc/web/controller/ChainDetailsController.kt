package com.nao4j.ccc.web.controller

import com.nao4j.ccc.domain.service.ChainLinkService
import com.nao4j.ccc.domain.service.ChainService
import com.nao4j.ccc.domain.service.ConversionService
import com.nao4j.ccc.domain.service.UserService
import com.nao4j.ccc.web.page.ChainDetailsPage
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/conversion-details/{conversionId}/chain-details/{chainId}")
class ChainDetailsController(
    private val conversionService: ConversionService,
    private val chainService: ChainService,
    private val linkService: ChainLinkService,
    private val userService: UserService
) {

    // TODO: handle not found
    @GetMapping
    fun getPage(
        @PathVariable conversionId: Long,
        @PathVariable chainId: Long,
        model: Model,
        authentication: Authentication
    ): String {
        val userId = userService.getIdByUsername(authentication.name)
        val chain = chainService.getDetails(userId!!, conversionId, chainId)
        val page = ChainDetailsPage(
            conversion = conversionService.getDetails(userId!!, conversionId),
            chain = chain,
            links = chain?.links?.map { linkService.getDetails(userId!!, conversionId, chainId, it.id) }
        )
        model.addAttribute("data", page)
        return "chain-details"
    }

    @PostMapping(params = ["action=add-link"])
    fun addLink(
        @PathVariable conversionId: Long,
        @PathVariable chainId: Long,
        @ModelAttribute("data") data: ChainDetailsPage,
        model: Model,
        authentication: Authentication
    ): String {
        val userId = userService.getIdByUsername(authentication.name)
        val chain = chainService.getDetails(userId!!, conversionId, chainId)
        // TODO: validate form
        val createdLink = linkService.create(
            userId = userId!!,
            conversionId = conversionId,
            chainId = chainId,
            parentId = chain?.links?.lastOrNull()?.id,
            rate = data.rate!!,
            percentageFee = data.percentageFee,
            fixedFee = data.fixedFee,
            currency = data.currency!!
        )
        val page = ChainDetailsPage(
            conversion = conversionService.getDetails(userId!!, conversionId),
            chain = chain,
            links = chain?.links?.map { linkService.getDetails(userId!!, conversionId, chainId, it.id) }?.plus(createdLink)
        )
        model.addAttribute("data", page)
        return "chain-details"
    }

    @PostMapping(params = ["action=delete"])
    fun delete(
        @PathVariable conversionId: Long,
        @PathVariable chainId: Long,
        @ModelAttribute("data") data: ChainDetailsPage,
        authentication: Authentication
    ): String {
        val userId = userService.getIdByUsername(authentication.name)
        chainService.getDetails(userId!!, conversionId, chainId)?.links?.lastOrNull()?.let { link ->
            linkService.delete(userId!!, conversionId, chainId, link.id)
        }
        return "redirect:/conversion-details/{conversionId}/chain-details/{chainId}"
    }

    @PostMapping(params = ["action=complete"])
    fun completeChain(
        @PathVariable conversionId: Long,
        @PathVariable chainId: Long,
        @ModelAttribute("data") data: ChainDetailsPage,
        model: Model,
        authentication: Authentication
    ): String {
        val userId = userService.getIdByUsername(authentication.name)
        val conversion = conversionService.getDetails(userId!!, conversionId)
        val error = try {
            chainService.complete(userId!!, conversionId, chainId)
            null
        } catch (e: Exception) {
            "Цепочка должна завершаться конвертацией в ${conversion?.currencyTo}"
        }
        val chain = chainService.getDetails(userId!!, conversionId, chainId)
        val page = ChainDetailsPage(
            conversion = conversion,
            chain = chain,
            links = chain?.links?.map { linkService.getDetails(userId!!, conversionId, chainId, it.id) },
            error = error
        )
        model.addAttribute("data", page)
        return "chain-details"
    }
}
