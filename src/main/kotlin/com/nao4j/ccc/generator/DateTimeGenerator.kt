package com.nao4j.ccc.generator

import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.ZoneId

@Component
class DateTimeGenerator {
    fun generateLocalDateTimeNow(): LocalDateTime = LocalDateTime.now(ZoneId.of("UTC"))
}
