package com.nao4j.ccc.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl
import javax.sql.DataSource

@Configuration
class WebSecurityConfig {

    @Bean
    fun userDetailsService(dataSource: DataSource): UserDetailsService =
        JdbcDaoImpl().apply {
            setDataSource(dataSource)
        }
}
