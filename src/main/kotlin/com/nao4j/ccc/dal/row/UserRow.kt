package com.nao4j.ccc.dal.row

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("users")
data class UserRow(
    @Id
    val id: Long,

    @Column("username")
    val username: String,

    @Column("enabled")
    val enabled: Boolean
)
