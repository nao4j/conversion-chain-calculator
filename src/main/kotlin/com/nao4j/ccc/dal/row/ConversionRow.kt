package com.nao4j.ccc.dal.row

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.math.BigDecimal
import java.time.LocalDateTime

@Table("conversions")
data class ConversionRow(
    @Id
    val id: Long? = null,

    @Column("user_id")
    val userId: Long,

    @Column("created_at")
    val createdAt: LocalDateTime,

    @Column("amount")
    val amount: BigDecimal,

    @Column("currency_from")
    val currencyFrom: String,

    @Column("rate")
    val rate: BigDecimal,

    @Column("currency_to")
    val currencyTo: String
)
