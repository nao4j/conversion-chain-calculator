package com.nao4j.ccc.dal.row

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.math.BigDecimal

@Table("chain_links")
data class ChainLinkRow(
    @Id
    val id: Long? = null,

    @Column("user_id")
    val userId: Long,

    @Column("conversion_id")
    val conversionId: Long,

    @Column("chain_id")
    val chainId: Long,

    @Column("parent_id")
    val parentId: Long? = null,

    @Column("rate")
    val rate: BigDecimal,

    @Column("percentage_fee")
    val percentageFee: BigDecimal? = null,

    @Column("fixed_fee")
    val fixedFee: BigDecimal? = null,

    @Column("currency")
    val currency: String
)
