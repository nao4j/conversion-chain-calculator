package com.nao4j.ccc.dal.row

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.math.BigDecimal

@Table("chains")
data class ChainRow(
    @Id
    val id: Long? = null,

    @Column("user_id")
    val userId: Long,

    @Column("conversion_id")
    val conversionId: Long,

    @Column("name")
    val name: String,

    @Column("status")
    val status: ChainStatusEnum = ChainStatusEnum.CREATED,

    @Column("loss_rate")
    val lossRate: BigDecimal? = null
) {
    enum class ChainStatusEnum {
        CREATED,
        IN_PROGRESS,
        COMPLETED
    }
}
