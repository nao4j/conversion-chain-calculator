package com.nao4j.ccc.dal.repository

import com.nao4j.ccc.dal.row.ConversionRow
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.Repository

interface ConversionRepository : Repository<ConversionRow, Long> {
    fun findAllByUserId(userId: Long, pageable: Pageable): Page<ConversionRow>

    fun findByUserIdAndId(userId: Long, id: Long): ConversionRow?

    fun save(conversion: ConversionRow): ConversionRow

    fun delete(conversion: ConversionRow)
}
