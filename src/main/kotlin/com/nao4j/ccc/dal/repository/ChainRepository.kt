package com.nao4j.ccc.dal.repository

import com.nao4j.ccc.dal.row.ChainRow
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.Repository

interface ChainRepository : Repository<ChainRow, Long> {
    fun findAllByUserIdAndConversionIdOrderByLossRateAscIdDesc(
        userId: Long,
        conversionId: Long,
        pageable: Pageable
    ): Page<ChainRow>

    fun findByUserIdAndConversionIdAndId(userId: Long, conversionId: Long, id: Long): ChainRow?

    fun save(chain: ChainRow): ChainRow

    fun delete(chain: ChainRow)
}
