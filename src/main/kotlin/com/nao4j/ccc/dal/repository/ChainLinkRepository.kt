package com.nao4j.ccc.dal.repository

import com.nao4j.ccc.dal.row.ChainLinkRow
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.Repository

interface ChainLinkRepository : Repository<ChainLinkRow, Long> {
    fun existsByUserIdAndConversionIdAndChainId(userId: Long, conversionId: Long, chainId: Long): Boolean

    fun findAllByUserIdAndConversionIdAndChainId(
        userId: Long,
        conversionId: Long,
        chainId: Long,
        pageable: Pageable
    ): Page<ChainLinkRow>

    fun findByUserIdAndConversionIdAndChainIdAndId(
        userId: Long,
        conversionId: Long,
        chainId: Long,
        id: Long
    ): ChainLinkRow?

    fun save(chainLink: ChainLinkRow): ChainLinkRow

    fun delete(chainLink: ChainLinkRow)
}
