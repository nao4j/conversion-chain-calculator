package com.nao4j.ccc.dal.repository

import com.nao4j.ccc.dal.row.UserRow
import org.springframework.data.repository.Repository

interface UserRepository : Repository<UserRow, Long> {

    fun findByUsername(username: String): UserRow?
}
