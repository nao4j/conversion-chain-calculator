CREATE TABLE chains (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGINT NOT NULL,
    conversion_id BIGINT NOT NULL,
    name VARCHAR(255) NOT NULL,
    status VARCHAR(32) NOT NULL,
    loss_rate NUMERIC(19, 2),
    FOREIGN KEY (user_id, conversion_id) REFERENCES conversions(user_id, id) ON DELETE CASCADE,
    UNIQUE (id, user_id, conversion_id)
);
