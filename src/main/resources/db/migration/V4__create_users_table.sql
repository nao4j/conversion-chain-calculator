CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(64) NOT NULL UNIQUE,
    password VARCHAR(512) NOT NULL,
    enabled BOOLEAN NOT NULL
);

ALTER TABLE conversions
    ADD CONSTRAINT fk_conversion_user FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE chains
    ADD CONSTRAINT fk_chain_user FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE chain_links
    ADD CONSTRAINT fk_chain_link_user FOREIGN KEY (user_id) REFERENCES users(id);
