CREATE TABLE conversions (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGINT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    amount NUMERIC(19, 8) NOT NULL,
    currency_from VARCHAR(128) NOT NULL,
    rate NUMERIC(19, 8) NOT NULL,
    currency_to VARCHAR(128) NOT NULL,
    UNIQUE (id, user_id)
);
