CREATE TABLE chain_links (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGINT NOT NULL,
    conversion_id BIGINT NOT NULL,
    chain_id BIGINT NOT NULL,
    parent_id BIGINT REFERENCES chain_links(id) ON DELETE CASCADE,
    rate NUMERIC(19, 8) NOT NULL,
    percentage_fee NUMERIC(19, 8),
    fixed_fee NUMERIC(19, 8),
    currency VARCHAR(128) NOT NULL,
    FOREIGN KEY (user_id, conversion_id, chain_id) REFERENCES chains(user_id, conversion_id, id) ON DELETE CASCADE,
    UNIQUE (id, user_id, conversion_id, chain_id)
);
