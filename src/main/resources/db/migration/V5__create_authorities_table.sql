CREATE TABLE authorities (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(64) NOT NULL REFERENCES users(username),
    authority VARCHAR(128) NOT NULL
)
