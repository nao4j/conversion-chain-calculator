INSERT INTO users (username, password, enabled)
VALUES
    ('admin', '{bcrypt}$2a$10$VkM3JD6q9zhfcY2Jl0McQuzjSqaGf.5T19pLcr3v9/xEFv4gaEoTi', TRUE),
    ('user', '{bcrypt}$2a$10$Uw3y4cKcohlvHuabgLNanuL.UZaE459SP0yEBC/c1kb9kRw1IeueK', TRUE);

INSERT INTO authorities (username, authority)
VALUES
    ('admin', 'ADMIN'),
    ('user', 'USER');
